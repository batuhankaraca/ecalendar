import React, { Component } from 'react'
import { View, Image, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
import Video from 'react-native-af-video-player';
import { Icon, Grid, Row, Spinner } from 'native-base';
import { DentStore } from './store';
import renderIf from './renderIf';

const { width } = Dimensions.get('window');


export default class AvView extends Component {
  state = {
    lastPress: 0,
    urlReady : 0,
    imgFailed : false,
    failedImg : require('../assets/images/no-image.jpg')
  }

  constructor(props) {
    super(props);
    this.state.post = DentStore.homeFeed.find(e=>e.POSTID===props.postId);

  }


    componentWillUnmount () {
      if (this.state.post.MEDIATYPEID === 2) {
        this.blurSubscription.remove();
      }
    }

  componentWillMount() {
    if (this.state.post.MEDIATYPEID === 1) {
      Image.getSize(this.state.post.MEDIAURL, (w, h) => {
        this.setState({ imageHeight: Math.floor(h * (width / w)) })
      },(a,b,c)=>{
        /*No image width*height 450*313 */
        this.setState({ imageHeight: Math.floor(313 * (width / 450)) })
      })
    }else{
      this.blurSubscription =
        this.props.navigation.addListener( 'willBlur', () => {
            if(this.player)
              this.player.pause();
          }
        )
    }
  }

  async fetchVideoUri(){
    if(this.state.urlReady===0){
      this.setState({urlReady:1});
      let url = await DentStore.fetchVideoUri(this.props.postId);
      this.setState({urlReady:2,fetchedUrl:url,autoPlay:true});
    }

  }

  render() {
    if (this.state.post.MEDIATYPEID === 1) {
      let imgSrc =this.state.imgFailed?this.state.failedImg: { uri: (this.state.post.MEDIAURL) };
      return (
        <View>
          <Image
            source={imgSrc}
            style={{ width, height: this.state.imageHeight }}
            resizeMode={'contain'}
            onError={(err)=>{this.setState({imgFailed:true})}}
          />
        </View>
      )
    }
    return (
      <TouchableOpacity
        onPress={() => { this.fetchVideoUri() }}
        activeOpacity={0.8}
      >
      {renderIf(this.state.urlReady===2)(
        <Video
          autoPlay
          ref={(ref) => {this.player = ref; }}
          url={this.state.fetchedUrl}
          logo={'http://www.1x1px.me/000000-0.png'}
          title={''}
          onLoad={(data) => {}}
          inlineOnly={true}
          scrollBounce={true}
          lockRatio={16 / 9}/>
      )}
      {renderIf(this.state.urlReady===1)(
        <Grid>
          <Row style={{alignItems:'center',justifyContent:'center'}}>
            <Spinner color='gray'  style={{height:320}}/>
          </Row>
        </Grid>
      )}
      {renderIf(this.state.urlReady===0)(
        <Row style={{justifyContent:'center',alignItems:'center'}}>
          <Image
            source={(this.state.post.THUMBURL?({ uri: this.state.post.THUMBURL }):(require('../assets/images/no-thumbnail.jpg')))}
            style={{ width, height: 320 }}
            resizeMode={'contain'}
          />
          <Icon name="play-circle-outline" type="MaterialIcons" style={{fontSize:80,color:'white',position:'absolute'}}/>
        </Row>
      )}
      </TouchableOpacity>
    )
  }
}
