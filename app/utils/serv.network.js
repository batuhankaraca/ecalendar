import { AsyncStorage } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';

const BASE_URL = 'https://dev.bendostum.net/dentinsta/api/';
const gBASE_URL = 'https://www.googleapis.com/upload/youtube/v3/videos?part=snippet%2Cstatus&uploadType=resumable';
const imgBB_URL = 'https://fatih-saymaz.imgbb.com/json';
const streamable_URL = 'https://api.streamable.com';
const streamable_TOKEN = 'bWlsdGZyZXh0MkBnbWFpbC5jb206TWltczEyMw==';
// const BASE_URL = 'http://192.168.1.17:6060/dentinsta/api/';


const defaultHttp = async(endPoint,params,method = 'POST') => {
  let dKey = await AsyncStorage.getItem('dKey');
  let encodedParam = Object.keys(params)
  .filter((k)=>{ return !(params[k]===null || params[k]==="null" || params[k]===undefined || params[k]==="undefined");})
  .map((key) => { return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]); }).join('&');

  // if(endPoint==='xwjgq'){
  //   if(__DEV__){ console.log( params ) }
  // }

  return (
    fetch(BASE_URL+endPoint, {
            method: method,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8','dKey':dKey },
            body: encodedParam,
    }));
};

const uploadContent = (type,uri) => {

}

export class ServNet {
  /* Test Call */
  static testEchoServer = (params) => {
    return defaultHttp('functionlist',params,'GET').then((res)=>{return res.json()});
  }

  static ytVideoUpload = (fileUrl,callbacks = {}) =>{
    RNFetchBlob.fs.stat(fileUrl)
    .then((stats) => { if(__DEV__){ console.log( stats ) }
      let token = 'ya29.Gl0PBko9Wv0lPAbFqdHsEJ5B3wO1YJveS7addohgh96CsLjG4ZG2JbVinrPupoGs6TPqC5-qOFb_qDBClqvRcXi1dKmO6Vnfugw_jtOQuRZWGBcnd4895bNxu3Cyvgw';
      let metaData = { snippet: { title: 'dentinsta', description: 'dentinsta-desc', tags: ['dentinsta-mobile'], categoryId: 22 }, status: { privacyStatus: 'unlisted' } };
      fetch(gBASE_URL, { method: "POST",
                        body: JSON.stringify(metaData),
                        headers: {'Authorization': `Bearer ${token}`,
                                  'Content-Type': 'application/json',
                                  'X-Upload-Content-Length': stats.size,
                                  'X-Upload-Content-Type': 'application/octet-stream' }
      })
      .then((resp)=>{
        if(__DEV__){ console.log( resp ) }
        if(resp.ok){
          this.url = resp.headers.get('Location');
          RNFetchBlob.fetch('PUT',resp.headers.get('Location'),
                            { 'Content-Type': 'application/octet-stream',
                              'Content-Range': `bytes 0-${stats.size - 1}/${stats.size}`,
                              'X-Upload-Content-Type': 'video/mp4'
                            },RNFetchBlob.wrap(fileUrl)
          )
          .uploadProgress((written, total) => {
              if (callbacks.hasOwnProperty('uploadProgress')) {
                callbacks.uploadProgress(parseFloat(written/total).toFixed(2));
              }
          })
          .then((resp) => { return resp.json(); })
          .then( (json) => {
            if (callbacks.hasOwnProperty('completeProgress')) {
              callbacks.completeProgress(json);
            }
          })
        }
      })
    }).catch((err) => { if(__DEV__){ console.log( errs ) } });

  }

  static mediaShare = (fileName, fileUrl,callbacks = {}  ,type = 0) => {
    /* default 0: Image,1 : Video */
    if(type === 0 ){
      RNFetchBlob.fetch('POST', imgBB_URL, { 'Content-Type' : 'octet-stream'}, [
        { name : 'source', filename :fileName , data: RNFetchBlob.wrap(fileUrl)},
        { name : 'type', data : 'file'},
        { name : 'action', data : 'upload'},
        { name : 'album_id', data : 'iriKMF'},
        { name : 'filename', data : fileName},
      ])
      .uploadProgress((written, total) => {
          if(callbacks.hasOwnProperty('uploadProgress'))
            callbacks.uploadProgress(parseFloat(written / total).toFixed(2));
      })
      .then((resp) => { return resp.json(); })
      .then((json) =>{
        if(callbacks.hasOwnProperty('completeProgress'))
          callbacks.completeProgress(json);
      })
      .catch((err) => {
        if(callbacks.hasOwnProperty('errProgress'))
          callbacks.errProgress(err);
      })
    }else{
      RNFetchBlob.fetch('POST', `${streamable_URL}/upload` , {
        'Authorization' : `Basic ${streamable_TOKEN}` ,
      }, [
        { name : 'file', filename :fileName , data: RNFetchBlob.wrap(fileUrl)},
        { name : 'filename', data : fileName},
      ])
      .uploadProgress((written, total) => {
        if(callbacks.hasOwnProperty('uploadProgress'))
          callbacks.uploadProgress(parseFloat(written / total).toFixed(2));
      })
      .then((resp) => { return resp.json() })
      .then((json) => {
        fetch(`${streamable_URL}/videos/${json.shortcode}`,{method: 'GET',headers:{'Authorization' : `Basic ${streamable_TOKEN}`}})
        .then(function(response){ return response.json();})
        .then((json)=>{
          if(__DEV__){ console.log( json ) }
          if(callbacks.hasOwnProperty('completeProgress'))
            callbacks.completeProgress(json);
        });
      }).catch((err) => {
        if(__DEV__){ console.log( err ) }
      })
    }

  }

  static fetchStreamableVideoUri = (uri) => {
    return (
      RNFetchBlob.fetch('GET', `${streamable_URL}/videos/${uri}` , {
        'Authorization' : `Basic ${streamable_TOKEN}` ,
      })
      .then(resp=>resp.json())
      .then(json=>{ return ('https:'+json.files['mp4-mobile'].url)})
      .catch(err=>{return ('https://fykmobile.com/video/video_not_found.mp4')}));

  };


  /* Generated via Google Sheets! */
  /*SECURITY*/
  static PKGSEC_LOGIN = (params) => {
  /* a:INAUTHTYPEID, b:INUSERKEY, c:INSECRET, d:INSOURCEKEY, e:INDEVICEKEY, f:INDEVICEDESC, g:INUSERAGENT, h:INCLIENTTYPEID, i:INCLIENTVERSION, j:INEMAIL, k:INNAME, l:INSURNAME, m:INNICKNAME, n:INAVATARURL, o:INIPV4, p:OUTUSERID, q:OUTSESSIONID, */
    return defaultHttp('puelg',params).then((res)=>{return res.json()});
  }
  static PKGSEC_LOGOUT = (params) => {
  /* a:INUSERID, b:INSESSIONID, */
    return defaultHttp('qbyfh',params).then((res)=>{return res.json()});
  }
  static PKGSEC_SETDEVICENTFTOKEN = (params) => {
  /* a:INDEVICEKEY, b:INNTFTOKEN, */
    return defaultHttp('pgxxw',params).then((res)=>{return res.json()});
  }
  /*OPERATION*/
  static PKGOP_ADDCOMMENT = (params) => {
  /* a:INPOSTID, b:INCOMMENTTEXT, c:INPARENTID, d:OUTISNEW, */
    return defaultHttp('mywhe',params).then((res)=>{return res.json()});
  }
  static PKGOP_CREATEPOST = (params) => {
  /* a:INMEDIATYPE, b:INPOSTTYPE, c:INTITLE, d:INPOSTTEXT, e:INMEDIAURL, f:INTHUMBURL, g:OUTPOSTID, */
    return defaultHttp('ubydb',params).then((res)=>{return res.json()});
  }
  static PKGOP_DELCOMMENT = (params) => {
  /* a:INPOSTID, b:INCOMMENTID, */
    return defaultHttp('bpntx',params).then((res)=>{return res.json()});
  }
  static PKGOP_DELETEPOST = (params) => {
  /* a:INPOSTID, */
    return defaultHttp('oxfyn',params).then((res)=>{return res.json()});
  }
  static PKGOP_GETFEEDLIST = (params) => {
  /* a:INPOSTTYPE, b:INTARGETID, c:INROWCOUNT, d:INCURRINDEX, e:OUTREC, */
    return defaultHttp('xwjgq',params).then((res)=>{return res.json()});
  }
  static PKGOP_GETPROFILE = (params) => {
  /* a:INUSERID, b:OUTREC, */
    return defaultHttp('llxvl',params).then((res)=>{return res.json()});
  }
  static PKGOP_GETPROFILEBYNICK = (params) => {
  /* a:INNICKNAME, b:OUTREC, */
    return defaultHttp('kvxlf',params).then((res)=>{return res.json()});
  }
  static PKGOP_HIDEPOST = (params) => {
  /* a:INPOSTID, */
    return defaultHttp('kitmg',params).then((res)=>{return res.json()});
  }
  static PKGOP_LIKEUNLIKE = (params) => {
  /* a:INPOSTID, b:INLIKESCORE, */
    return defaultHttp('vmmfh',params).then((res)=>{return res.json()});
  }
  static PKGOP_LISTALERT = (params) => {
  /* a:INROWCOUNT, b:INCURRINDEX, c:OUTREC, */
    return defaultHttp('degbp',params).then((res)=>{return res.json()});
  }
  static PKGOP_LISTCOMMENT = (params) => {
  /* a:INPOSTID, b:INROWCOUNT, c:INCURRINDEX, d:OUTREC, */
    return defaultHttp('hvlvu',params).then((res)=>{return res.json()});
  }
  static PKGOP_REPORTPOST = (params) => {
  /* a:INPOSTID, */
    return defaultHttp('jylhc',params).then((res)=>{return res.json()});
  }
  static PKGOP_SETAVATAR = (params) => {
  /* a:INAVATARURL, */
    return defaultHttp('cjhea',params).then((res)=>{return res.json()});
  }
  static PKGOP_SETNICKNAME = (params) => {
  /* a:INNICKNAME, */
  return defaultHttp('eizro',params).then((res)=>{return res.json()});
  }
  static PKGOP_SETPROFILE = (params) => {
  /* a:INNAME, b:INSURNAME, c:INBIRTHDATE, d:INEMAIL, e:INMOBILE, f:INNICKNAME, g:INAVATARURL, */
    return defaultHttp('tottk',params).then((res)=>{return res.json()});
  }

}
