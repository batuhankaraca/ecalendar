import mobx, { observable,action,computed } from "mobx";
import { AsyncStorage, NativeModules } from 'react-native';
import { ServNet } from './serv.network';

const baseUtil = NativeModules.RNMobileBaseUtil;
const loadCount = 10;
class Store {
    constructor(){
      AsyncStorage.getItem('osUserId').then((val)=>{this.oldToken = val;});
    }

    @observable homeFeed=[];
    @observable homeLoadable = true;

    @observable advFeed=[];
    @observable advLoadable = true;

    @observable actvtFeed=[];
    @observable actvtLoadable = true;

    @observable personalFeed=[];
    @observable personalLoadable = true;

    @observable profileData = null;



    @action('loadProfile')
    async loadProfile(userId){
      let res = await ServNet.PKGOP_GETPROFILE({a:userId});
      this.profileData = res.b[0];
    }


    @action('loadFeed')
    async loadFeed(type,targetId,refreshFlag=false){
      /*1:Post 2:Ad 3:Event*/
      /* a:INPOSTTYPE, b:INTARGETID, c:INROWCOUNT, d:INCURRINDEX, e:OUTREC, */
      if(refreshFlag){
        switch (type) {
          case 1:
            this.homeLoadable = true;
            this.homeFeed = [] ;
          break;
          case 2:
            this.advLoadable = true;
            this.advFeed = [] ;
          break;
          case 3:
            this.actvtLoadable = true;
            this.actvtFeed = [] ;
          break;

        }
      }
      if(targetId){
        alert('not implemented!');
      }
      let reqInfo = {a:type,b:targetId,c:loadCount}
      let l = 0;
      switch (type) {
        case 1:
          l = this.homeFeed.length;
          reqInfo.d = l===0?l:this.homeFeed[(l-1)].POSTID;
        break;
        case 2:
          l = this.advFeed.length;
          reqInfo.d = l===0?l:this.advFeed[(l-1)].POSTID;
        break;
        case 3:
          l = this.actvtFeed.length;
          reqInfo.d = l===0?l:this.actvtFeed[(l-1)].POSTID;
        break;
      }
      let srvRes = await ServNet.PKGOP_GETFEEDLIST(reqInfo);
      if(srvRes){
        let arr = [];
        switch (type) {
          case 1:
            arr = this.homeFeed.concat(srvRes.e);
            this.homeLoadable = srvRes.e.length===loadCount;
            this.homeFeed = arr ;
          break;
          case 2:
            arr = this.advFeed.concat(srvRes.e);
            this.advLoadable = srvRes.e.length===loadCount;
            this.advFeed = arr ;
          break;
          case 3:
            arr = this.actvtFeed.concat(srvRes.e);
            this.actvtLoadable = srvRes.e.length===loadCount;
            this.actvtFeed = arr ;
          break;
        }
      }
      // let resp = await ServNet.PKGOP_GETFEEDLIST({a:type,b:targetId,c:rowCount,d:curCount});
    }

    @action('fetchVideoUri')
    async fetchVideoUri(postId,type){
      let found = this.homeFeed.find(e=>e.POSTID===postId);
      if(found ){
        if(!found.FETCHEDURI){
          let m = found.MEDIAURL;
          let media = m.substring((m.indexOf('/')+1),(m.length));
          let streamRes = await ServNet.fetchStreamableVideoUri(media);
          found.FETCHEDURI = streamRes;
          return found.FETCHEDURI;
        }else{
          return found.FETCHEDURI;
        }
      }
    }

    @action('addNewFeed')
    async addNewFeed(){
        // let resp = await ServNet.PKGOP_CREATEPOST()
    }

    @action('deletePost')
    deletePost(postId){
      let p = this.homeFeed.find(e=>e.POSTID===postId);
      let ix = this.homeFeed.findIndex(e=>e.POSTID===postId);
      if(p.ISMINE){
        ServNet.PKGOP_DELETEPOST({a:postId});
        this.homeFeed.splice(ix,1);
      }
    }

    @action('hidePost')
    hidePost(postId){
      let ix = this.homeFeed.findIndex(e=>e.POSTID===postId);
      if(ix>-1){
        ServNet.PKGOP_HIDEPOST({a:postId});
        this.homeFeed.splice(ix,1);
      }
    }

    @action('reportPost')
    reportPost(postId){
      let p = this.homeFeed.find(e=>e.POSTID===postId);
      if( p && !p.REPORTED){
        ServNet.PKGOP_REPORTPOST({a:postId});
        p.REPORTED = true;

      }
    }

    @action("updateLikeCommCount")
    async updateLikeCommCount(params){
      let {typeLike,postId,comment,commentId} = params;
      let found=this.homeFeed.find(e => e.POSTID===postId);
      if(found){
        if(!typeLike){
          if(commentId){
            ServNet.PKGOP_DELCOMMENT({a:postId,b:commentId});
            found.COMMENTCOUNT--;
          }else{
            let srvRes = await ServNet.PKGOP_ADDCOMMENT({a:postId,b:comment});
            if(srvRes.d)
              found.COMMENTCOUNT++;
          }
        }else{
          let action = 0;
          if(found.ISLIKED){
            found.LIKECOUNT--;
          }else{
            found.LIKECOUNT++;
            action = 1;
          }
          found.ISLIKED=!found.ISLIKED;
          ServNet.PKGOP_LIKEUNLIKE({a:found.POSTID,b:action})
        }

      }
    }


    @action('oneSignalStatus')
    async setOneSignal(val){
      if(val.userSubscriptionEnabled && val.userId != this.oldToken ){
        let { UID } = await baseUtil.getDeviceUID();
        ServNet.PKGSEC_SETDEVICENTFTOKEN({a:UID,b:val.userId});
        AsyncStorage.setItem('osUserId',val.userId);
      }
    }

}

export const DentStore = new Store()
