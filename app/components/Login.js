import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  KeyboardAvoidingView,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';

import {StackNavigator} from 'react-navigation';



export default class Login extends React.Component {
  constructor(props){
  super(props);
  this.state = {
    username : '',
    password: '',
  }
}
saveUser =() =>{
  const {username,password} = this.state;
  let user = {
    username : username,
    password : password,

  }
  AsyncStorage.setItem('user', JSON.stringify(user));

}

displayUser = async () => {
  try {
      let user = await AsyncStorage.getItem('user');
      let parsed = JSON.parse(user);
      console.log(parsed);
  } catch (e) {}
}



  login = () => {
    this.saveUser();
    this.displayUser();
    this.props.navigation.navigate('Home');
  }

  render(){
    return (

      <KeyboardAvoidingView behavior = 'padding' style={styles.wrapper}>

        <View style = {styles.container}>

          <Text style = {styles.header}> GİRİŞ </Text>

          <TextInput style = {styles.textInput} placeholder = 'Username'
          onChangeText = {(username) => this.setState({username})}

          />
          <TextInput style = {styles.textInput} placeholder = 'Password'
          onChangeText = {(password) => this.setState({password})}

          />

          <TouchableOpacity style ={ styles.btn}
                            onPress = {()=>{
                              this.login();
                            }} >
                            <Text style ={styles.header}>Giriş</Text>
                            </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    );
  }

}
const styles = StyleSheet.create({
  wrapper: {
    flex:1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F6F8E2',
    paddingLeft: 40,
    paddingRight: 40,
  },

  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },

  textInput:{
    alignSelf: 'stretch',
    alignItems:'center',
    padding: 10,
    marginBottom: 30,
    backgroundColor :'#F6F8E2',
  },

  btn: {
    alignSelf: 'stretch',
    alignItems:'center',
    padding: 15,
    backgroundColor :'#F6F8E2',

  },
  header :{
    fontSize: 20,
    marginBottom: 30,
    color: '#333333',
    fontWeight: 'bold'
  }

});
// <Text>Ad</Text>
// <TextInput style = {styles.textInput} value = {parsed.ad}
// onChangeText = {(ad) => this.setState({ad})}
//
// />
//
// <TextInput style = {styles.textInput} value = {parsed.soyad}
// onChangeText = {(soyad) => this.setState({soyad})}
//
// />
// <TextInput style = {styles.textInput} value = {parsed.email}
// onChangeText = {(email) => this.setState({email})}
//
// />
// <TextInput style = {styles.textInput} value = {parsed.tel1}
// onChangeText = {(tel1) => this.setState({tel1})}
//
// />
// <TextInput style = {styles.textInput} value = {parsed.tel2}
// onChangeText = {(tel2) => this.setState({tel2})}
//
// />
// <TextInput style = {styles.textInput} value = {parsed.birthDay}
// onChangeText = {(birthDay) => this.setState({birthDay})}
//
// />
