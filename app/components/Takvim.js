import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  Content,ScrollView,
  Dimensions,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  KeyboardAvoidingView
} from 'react-native';
import { Agenda } from 'react-native-calendars';
import {LocaleConfig} from 'react-native-calendars';
import moment from 'moment';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import {  Tab, Icon } from 'react-native-elements'
const DEVICE_WIDTH = Dimensions.get(`window`).width;
import PopupDialog, { SlideAnimation,DialogButton } from 'react-native-popup-dialog';
import renderIf from '../utils/renderIf';
import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import {Button} from 'native-base';
import Toast from 'react-native-whc-toast'


const Loading =true;
var count =0;
var timeArr = [];
var options = [];
var CANCEL_INDEX =4;
const AsyncUser = false;
const parsed = [];
const a= 0;


LocaleConfig.locales['tr'] = {
  monthNames: ['Ocak','Şubat','Mart','Nisan','Mayıs','Haziran','Temmuz','Ağustos','Eylül','Ekim','Kasım','Aralık'],
  monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
  dayNames: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
  dayNamesShort: ['Paz','Ptsi','Sal','Çar','Per','Cum','Ctsi']
};

LocaleConfig.defaultLocale = 'tr';

const sampleCustomerData = {"COSTUMERID":1,"NAME":"BATUHAN","COORDX":38.394417,"COORDY":27.03573,"PHONE1":5416181893,"PHONE2":null,"NOTABENE":"abc","SECTORID":3,"DAYSTARTS":"9:00","DAYENDS":"18:00","DAY1":0,"DAY2":1,"DAY3":1,"DAY4":1,"DAY5":1,"DAY6":1,"DAY7":1,"SLOTMINS":"120"};
const sampleCompanyData = {"COMPANYID":1,"SEATCOUNT":1,"ADDRESS":"İnciraltı Mah.","CITYID":35,"DISTRICTNAME":"Balçova","COORDX":38.394417,"COORDY":27.03573,"PHONE1":5416181893,"PHONE2":null,"NOTABENE":"abc","SECTORID":3,"DAYSTARTS":"9:00","DAYENDS":"18:00","DAY1":1,"DAY2":1,"DAY3":1,"DAY4":1,"DAY5":1,"DAY6":0,"DAY7":1,"SLOTMINS":"120"};
const PauseDay = [sampleCompanyData.DAY7,sampleCompanyData.DAY1,sampleCompanyData.DAY2,sampleCompanyData.DAY3,sampleCompanyData.DAY4,sampleCompanyData.DAY5,sampleCompanyData.DAY6];
const tab=[0];
const slideAnimation = new SlideAnimation({
            slideFrom: 'bottom',
          });


export default class Takvim extends Component {
  constructor(props) {
    super(props);
    this.state = {
       items: {},
       dialogShow: false,
       ad : '',
       soyad: '',
       tel1: '',
       email: '',
    };
  }
  showActionSheet = () => {
    if(options.length>2)
   this.ActionSheet.show()
   else {
     if(AsyncUser== false)
       this.popupDialog.show();
       else
       this.popupDialog1.show();
     }
   }


  seatCounts =() => {
    for(let i =1; i<=sampleCompanyData.SEATCOUNT; i++){

      options[i-1]= i;
      // console.log(options);
    }
    options[options.length]= 'İPTAL';
    // console.log(options);
     CANCEL_INDEX = options.length;

  }
  calcTimeBox = () => {

    let start = moment(sampleCompanyData.DAYSTARTS, "HH:mm");
    let end = moment(sampleCompanyData.DAYENDS, "HH:mm");
    let dur = sampleCompanyData.SLOTMINS;

    let isInTime = true;
    // console.log(start.format("HH:mm"));
    // console.log(end.diff(start));
    while (isInTime) {
      var sTime = start.format("HH:mm")
      start.add(moment.duration(parseInt(dur), 'minutes'));
      timeArr.push("  "+sTime+"~"+start.format("HH:mm"))

      count++;
      if(end.diff(start)<=0){
        isInTime = !isInTime;
      }
    }
//    console.log(timeArr);
        Loading=false;
  }

  onDayPress(day) {
    this.setState({
        selected: day.dateString
      });
    }

  componentDidMount(){
    // console.log(AsyncUser);
    this.displayUser();
    if(Loading==true){
      this.calcTimeBox();
      this.seatCounts();
    }
  }

  saveUser = () =>{
    const {ad,soyad,tel1,email} = this.state;
    let user = {
      ad : ad,
      soyad: soyad,
      phone: tel1,
      email: email,
    }
    if(this.state.ad && this.state.soyad && this.state.tel1 && this.state.email ){
    AsyncStorage.setItem('user', JSON.stringify(user));
  }
  else{
    // console.log("kaydetmedi");
  }
    // console.log(AsyncUser);
    this.displayUser();
  }

  displayUser = async () => {
    try {
        let user = await AsyncStorage.getItem('user');
        parsed = JSON.parse(user);
        if(parsed != null)
        AsyncUser = true;
        // console.log(parsed);
    } catch (e) {}
  }
  // navigateScreen = () => {
  //   console.log("aaa");
  //   this.props.navigation.navigate('Setting');
  // }

  checkTab =(index) => {
      tab[0]= index +1;
      // console.log(tab[0]);
      if(index+1 != options.length){
      if(AsyncUser== false)
        this.popupDialog.show();
        else
        this.popupDialog1.show();
      }
    };

  render() {
    // console.log(LocaleConfig.locales.tr.dayNames[0] +  "        ssss");
    const today = new Date();

    // console.log(PauseDay);
    // const {navigate} = this.props.navigation


    return (
      <KeyboardAvoidingView behavior = 'padding' keyboardVerticalOffset={-5} style={styles.wrapper}>
<View style={styles.container}>
  {renderIf(tab[0]===0)(
     <Agenda
          pastScrollRange={0}
          minDate={(today)}
          firstDay={1}
          items={this.state.items}
          onDayPress = {(day) => { /*console.log('selected day', day);*/ this.setState(day)}}
          loadItemsForMonth={this.loadItems.bind(this)}
          renderItem={this.renderItem.bind(this)}
          renderEmptyDate={this.renderEmptyDate.bind(this)}
          rowHasChanged={this.rowHasChanged.bind(this)}
          renderEmptyDate={this.renderEmptyDate.bind(this)}
      />)}
    {renderIf(tab[0]===1)(
        <Agenda
          pastScrollRange={0}
          minDate={(today)}
          firstDay={1}
          items={this.state.items}
          onDayPress = {(day) => {console.log('selected day', day); this.setState(day)}}
          loadItemsForMonth={this.loadItems.bind(this)}
          renderItem={this.renderItem.bind(this)}
          renderEmptyDate={this.renderEmptyDate.bind(this)}
          rowHasChanged={this.rowHasChanged.bind(this)}
          renderEmptyDate={this.renderEmptyDate.bind(this)}
          />
    )}
    {renderIf(tab[0]===2)(
        <Agenda
          pastScrollRange={3}
          minDate={(today)}
          firstDay={1}
          items={this.state.items}
          onDayPress = {(day) => {console.log('selected day', day); this.setState(day)}}
          loadItemsForMonth={this.loadItems.bind(this)}
          renderItem={this.renderItem.bind(this)}
          renderEmptyDate={this.renderEmptyDate.bind(this)}
          rowHasChanged={this.rowHasChanged.bind(this)}
          renderEmptyDate={this.renderEmptyDate.bind(this)}
          />
      )}
    {renderIf(tab[0]===3)(
        <Agenda
            pastScrollRange={0}
            minDate={(today)}
            firstDay={1}
            items={this.state.items}
            onDayPress = {(day) => {console.log('selected day', day); this.setState(day)}}
            loadItemsForMonth={this.loadItems.bind(this)}
            renderItem={this.renderItem.bind(this)}
            renderEmptyDate={this.renderEmptyDate.bind(this)}
            rowHasChanged={this.rowHasChanged.bind(this)}
            renderEmptyDate={this.renderEmptyDate.bind(this)}
        />
        )}
    {renderIf(tab[0]===4)(
        <Agenda
              pastScrollRange={0}
              minDate={(today)}
              firstDay={1}
              items={this.state.items}
              onDayPress = {(day) => {console.log('selected day', day); this.setState(day)}}
              loadItemsForMonth={this.loadItems.bind(this)}
              renderItem={this.renderItem.bind(this)}
              renderEmptyDate={this.renderEmptyDate.bind(this)}
              rowHasChanged={this.rowHasChanged.bind(this)}
              renderEmptyDate={this.renderEmptyDate.bind(this)}
        />
          )}

        <PopupDialog
                height = {350}
                overlayOpacity = {0.8}
                ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                dialogAnimation={slideAnimation} >
                    <View style={styles.dialogContentView}>
                        <Text>Ad</Text>
                          <TextInput style = {styles.textInput} placeholder = ''
                          onChangeText = {(ad) => this.setState({ad})}/>
                        <Text>Soyad</Text>
                          <TextInput style = {styles.textInput} placeholder = ''
                          onChangeText = {(soyad) => this.setState({soyad})}/>
                        <Text>Telefon</Text>
                          <TextInput style = {styles.textInput} placeholder = ''
                          onChangeText = {(tel1) => this.setState({tel1})}/>
                        <Text>E-Mail</Text>
                          <TextInput style = {styles.textInput} placeholder = ''
                          onChangeText = {(email) => this.setState({email})}/>
                        <Button
                          onPress={this.saveUser}><Text>Kayıt</Text></Button>
                    </View>
        </PopupDialog>


        <PopupDialog
                ref={(popupDialog1) => { this.popupDialog1 = popupDialog1; }}
                dialogAnimation={slideAnimation} >
                    <View style={styles.dialogContentView}>
                        <Text>Not</Text>
                        <TextInput style = {styles.textInput} placeholder = 'Not...'/>
                        <Button onPress={()=>{
                        this.refs.toast.show('Bilgiler Kaydedildi');
                    }}><Text>Kayıt</Text></Button>
                    </View>

        </PopupDialog>
        <Toast
            ref="toast"

            fadeInDuration = {300}
            positionValue = {100}/>

</View>
</KeyboardAvoidingView>
          );
  }

renderEmptyDate() {
    return (
      <View style={styles.emptyDate}><Text>Boş Gün!</Text></View>
    );
  }


  loadItems(day) {
    // console.log(day);
    var weekDayName  = moment(day.dateString);
    var dow  = weekDayName.day() ;
    var getfuture = 8- dow;
    // console.log("Dddddd " + weekday1);
    // console.log("aaaaa " +weekDayName);
     // console.log("bbbbb " + dow);
    var deneme = dow;

    setTimeout(() => {
      for (let i = 0; i <getfuture; i++) { //10
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
      // console.log(time);
        const strTime = this.timeToString(time);
        a = dow%7;
       // console.log("tatil:   " + a);
       dow++;

      // console.log(strTime);
        if (!this.state.items[strTime]) {

          this.state.items[strTime] = [];

              // console.log(dow);

              // console.log(dow);
              // console.log(PauseDay[dow]);
               // console.log(PauseDay);

               // console.log("PauseDay  :" +PauseDay[a]);
          if(PauseDay[a] != 0 ){

            const numItems = count;
          for (let j = 0; j < numItems; j++) {
              this.state.items[strTime].push({
                name : timeArr[j],
                height: 60,
                });
      // console.log(this.state.items[strTime]);
              }
            }

          }
        }
      // console.log(this.state.items);
      const newItems = {};
      Object.keys(this.state.items).forEach(key => {newItems[key] = this.state.items[key];});
        this.setState({
          name: 'aaa',
          items: newItems
        });
      }, 1000);
      // console.log(`Load Items for ${day.year}-${day.month}`);
  }

  renderItem(item) {
      // console.log(item);
      return (
        <View style={[styles.item, {height: item.height}]}>
          <Button style = {styles.btn}
            onPress={this.showActionSheet}
            // onPress={this.slideAnimation}
          >
          <ActionSheet
                ref={o => this.ActionSheet = o}
                title={<Text style={{color: '#000', fontSize: 18}}>Which one do you like?</Text>}
                options={options}
                cancelButtonIndex={options[options.length]}
                onPress={(index) => {this.checkTab(index)}}
        />
            <Text>{item.name}</Text>
          </Button>
        </View>
      );
  }
  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerRight: <Button transparent onPress = {() => {navigation.navigate('Setting')}} >
  <Icon name ='person'/></Button>,
});
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F6F8E2',
  },
  wrapper: {
    flex:1,
  },

  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 50,
    padding: 7,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  },
  textInput:{
    alignSelf: 'stretch',
    alignItems:'center',
    padding: 10,
    marginBottom: 10,
    backgroundColor :'#F6F8E2',
  },

  dialogContentView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 40,
    paddingRight: 40,
  },
  btn: {
    width: 290,
    borderRadius: 50
  }

});
