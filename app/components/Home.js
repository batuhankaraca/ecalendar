import React, { Component } from 'react';
import { Container, Text, View, Input,Button,Item,TextInput} from 'native-base';
import {StyleSheet,ScrollView,TouchableOpacity,Dimensions,StatusBar,KeyboardAvoidingView, ActivityIndicator,TouchableWithoutFeedback,Keyboard, FlatList, ListView, CheckBox, Picker,Animated,NativeModules } from 'react-native';
import { List, ListItem } from 'react-native-elements'
const DEVICE_WIDTH = Dimensions.get(`window`).width;
import {StackNavigator} from 'react-navigation';
import { SearchableFlatList } from "react-native-searchable-list";
import Search from 'react-native-search-box';
import renderIf from '../utils/renderIf';
import { Dropdown } from 'react-native-material-dropdown';
import { getStatusBarHeight } from 'react-native-status-bar-height';

const baseUtil = NativeModules.RNMobileBaseUtil;

const datanet = [];

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})


export default class Home extends React.Component {

  constructor(props){
  super(props);

  this.state = {

      focus: false,
      Sehir: '',
      Semt : '',
      data: [
          {CITYID: 3,  CITYNAME: "Afyon" },
          {CITYID: 5,  CITYNAME: "Amasya"},
          {CITYID: 6,  CITYNAME: "Ankara"},
          {CITYID: 68, CITYNAME: "Aksaray"},
          {CITYID: 7,  CITYNAME: "Antalya"},
          {CITYID: 75, CITYNAME: "Ardahan"},
          {CITYID: 8,  CITYNAME: "Artvin"},
          {CITYID: 9,  CITYNAME: "Aydın"}, ],
      data2: [
          {SEMTID: 0,  SEMTNAME: "" },
          {SEMTID: 3,  SEMTNAME: "a" },
          {SEMTID: 5,  SEMTNAME: "b"},
          {SEMTID: 6,  SEMTNAME: "c"},
          {SEMTID: 68, SEMTNAME: "d"},
          {SEMTID: 7,  SEMTNAME: "e"},
          {SEMTID: 75, SEMTNAME: "f"},
          {SEMTID: 8,  SEMTNAME: "g"},
          {SEMTID: 9,  SEMTNAME: "h"}, ],
      searchTerm: "",
      searchAttribute: "CITYNAME",
      ignoreCase: true,
      loadingflag : true,
      emptySearch: true,

    };
  }

  DismissKeyboard = () => {
        return (
          <TouchableWithoutFeedback onPress={Keyboard.dismiss()}>
          </TouchableWithoutFeedback>
        )
  }
    handleInputFocus = () => {
        // this.deneme();
        let  UID  =  baseUtil.getDeviceUID();
        console.log(UID);

        this.setState({ focus: true });
        // console.log(getStatusBarHeight());


        // console.log(this.state.focus);

        // this.setState({searchAttribute:"name"});
      };
      handleInputUnFocus = () => {
          this.DismissKeyboard();
          this.setState({ focus: false });
          // console.log(this.state.focus);
          // console.log(this.state.searchTerm);
          // console.log(this.state.Sehir);
          // console.log(this.state.Semt);
        };

      navigateScreen = () => {

        this.props.navigation.navigate('Takvim');
      }
      compenentWillMount(){
        this.flagControll();
      }
      componentDidMount(){
        this.displayUser();
      }



      flagControll = (searchTerm) => {
        this.setState({ searchTerm : searchTerm});
              this.setState({emptySearch: false})
              this.setState({loadingflag : true })
          // console.log(loadingflag);
          setTimeout(() => {
            this.setState({loadingflag : false})
            // console.log(this.state.loadingflag);
          }, 1000)
      }
      displayUser = async () => {
        try {
            let user = await AsyncStorage.getItem('user');
            parsed = JSON.parse(user);
            // if(parsed != null)
            //
            // console.log(parsed.ad);
        } catch (e) {}
      }

     _renderItem = ({ item }) => {
              // console.log("asfsf");
                  return (<ListItem
                          title ={item.CITYNAME}
                          onPress = {()=>{
                            this.navigateScreen();}}
                          containerStyle= {{borderBottomWidth:0}}/>)
        }



static navigationOptions = {
      header:null,
  };

  render(){



    {navigate= this.props.navigation.navigate}
     const { data,data2, searchTerm, searchAttribute, ignoreCase } = this.state;
     // console.log(this.state.loadingflag);
    return (

<KeyboardAvoidingView behavior = 'padding' style={styles.wrapper}>

    <Animated.View  style = {styles.header}>

      <Search
      onFocus= {this.handleInputFocus}
      onChangeText={this.flagControll}
      onCancel= {this.handleInputUnFocus}
      placeholder="Ara"
      cancelTitle= "İptal"
      backgroundColor = "grey"
      titleCancelColor= "black"
      />

    </Animated.View>

          {renderIf(this.state.focus==true)(

    <View>
    <View style={{ flexDirection: 'row' }}>
          <View style={{ width:DEVICE_WIDTH/2,flex: 1 }}>
            <Dropdown
              label='Şehir'
              onChangeText = {(itemValue, itemIndex) => this.setState({Sehir:itemValue})}
              data={data}
              valueExtractor={(item,ix)=>{ return item.CITYNAME;}}
            />
        </View>
        <View style={{ width: DEVICE_WIDTH/2, marginLeft: 8 }}>
          <Dropdown
            label='Semt'
            onChangeText = {(itemValue, itemIndex) => this.setState({Semt:itemValue})}
            data={data2}
            valueExtractor={(item,ix)=>{ return item.SEMTNAME;}}
          />
        </View>
    </View>

  {renderIf(this.state.loadingflag==true && this.state.emptySearch==false)(
    <View style = {styles.spinner}>
  <ActivityIndicator color ='black' />
    </View>
  )}

{renderIf(this.state.loadingflag==false)(
  <ScrollView>
  <SearchableFlatList
      style={styles.list}
      data={data}
      searchTerm={searchTerm}
      ignoreCase={ignoreCase}
      searchAttribute={searchAttribute}
      renderItem={this._renderItem}/>
      </ScrollView>
)}


      </View>

)}
<ScrollView>{renderIf(this.state.focus==false)(
  <View style ={styles.wrapper}>
      <Container style ={styles.container}>
        <TouchableOpacity style = {styles.btn}
              onPress = {()=>{
              navigate('Dukkan', {id:1})}}>
              <Text style ={styles.txt}>DENEME</Text>
        </TouchableOpacity>

        <TouchableOpacity style = {styles.btn}
              onPress = {()=>{
              navigate('Dukkan', {id:2})}}>
              <Text style ={styles.txt}>DENEME</Text>
        </TouchableOpacity>
     </Container>

     <Container style ={styles.container}>
        <TouchableOpacity style = {styles.btn}
              onPress = {()=>{
              navigate('Takvim', {id:3})}}>
              <Text style ={styles.txt}>DENEME</Text>
        </TouchableOpacity>

        <TouchableOpacity style = {styles.btn}
              onPress = {()=>{
              navigate('EventCalendar', {id:4})}}>
              <Text style ={styles.txt}>DENEME</Text>
        </TouchableOpacity>
     </Container>

     <Container style ={styles.container}>
        <TouchableOpacity style = {styles.btn}onPress = {()=>{
              navigate('Dukkan', {id:5})}}>
              <Text style ={styles.txt}>DENEME</Text>
        </TouchableOpacity>

        <TouchableOpacity style = {styles.btn}
              onPress = {()=>{
              navigate('Dukkan', {id:6})}}>
              <Text style ={styles.txt}>DENEME</Text>
        </TouchableOpacity>
     </Container>

     <Container style ={styles.container}>
        <TouchableOpacity style = {styles.btn}
              onPress = {()=>{
              navigate('Dukkan', {id:7})}}>
              <Text style ={styles.txt}>DENEME</Text>
        </TouchableOpacity>

        <TouchableOpacity style = {styles.btn}
              onPress = {()=>{
              navigate('Dukkan', {id:8})}}>
              <Text style ={styles.txt}>DENEME</Text>
        </TouchableOpacity>
      </Container>
  </View> )}
</ScrollView>
</KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
      flex:1,
      backgroundColor: '#F6F8E2',
    },

  container:{
      height:170,
      flexDirection:'row',
      backgroundColor: '#F6F8E2',
      paddingTop: 15,
      marginBottom: 5,
    },

    header: {
      paddingTop:getStatusBarHeight(),
      width: DEVICE_WIDTH,
    },

  txt:{
      color: '#2c3e50',
      alignItems:'center',
    },

  spinner: {
      flex: 1,
      justifyContent: 'center',
      paddingTop: getStatusBarHeight()*7,
      alignItems: 'center',
    },

  btn:{
      alignItems:'center',
      borderRadius: 40,
      justifyContent:'center',
      backgroundColor:'#f1c40f',
      height: 150,
      width:160,
      marginLeft:18,
      marginTop:10,
    }

})
