import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  ScrollView,
  Text,
  View,
  TextInput,
  Button,
  KeyboardAvoidingView,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {Container} from 'native-base';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import DatePicker from 'react-native-datepicker'

import {StackNavigator} from 'react-navigation';

const parsed = [];

export default class Setting extends React.Component {
  constructor(props){
  super(props);
  this.state = {
    ad   : "",
    soyad:"" ,
    email:"",
    tel1 : "",
    tel2 : "",
    birthDay: "2016-05-15",
  }
}

saveUser = () =>{
      const {ad,soyad,email,tel1,tel2,birthDay} = this.state;
      let user = {
        ad : ad,
        soyad: soyad,
        tel1: tel1,
        tel2: tel2,
        email: email,
        birthDay: birthDay,
      }
      if(this.state.ad && this.state.soyad && this.state.tel1 ){
        AsyncStorage.setItem('user', JSON.stringify(user));
      }
      else{
        // console.log("oldu");
      }

      // console.log(ad);
      // console.log(AsyncUser);
      this.displayUser();
    }

 async componentDidMount(){
       await this.displayUser().done();

}

displayUser = async () => {
  try {
      let user = await AsyncStorage.getItem('user');
      parsed =   JSON.parse(user);
       // if(parsed != null)
       // console.log(parsed);
      this.setState({ad:parsed.ad, soyad:parsed.soyad, email:parsed.email, tel1:parsed.tel1, tel2:parsed.tel2, birthDay:parsed.birthDay});
  } catch (e) {}
}

  render(){
    return(

      <KeyboardAvoidingView behavior = 'padding'  style={styles.wrapper}>
<ScrollView>
        <View style = {styles.container}>
        <FormLabel>Ad</FormLabel>
        <FormInput
          value = {this.state.ad}
          onChangeText={(ad) => this.setState({ad})}/>
        <FormValidationMessage>{'Alan Doldurulmalı'}</FormValidationMessage>

        <FormLabel>Soyad</FormLabel>
        <FormInput
          value = {this.state.soyad}
          onChangeText={(soyad) => this.setState({soyad})}/>
        <FormValidationMessage>{'Alan Doldurulmalı'}</FormValidationMessage>

        <FormLabel>E-Mail</FormLabel>
        <FormInput
          value = {this.state.email}
          onChangeText={(email) => this.setState({email})}/>

        <FormLabel>Telefon 1</FormLabel>
        <FormInput
          value = {this.state.tel1}
          onChangeText={(tel1) => this.setState({tel1})}/>
        <FormValidationMessage>{'Alan Doldurulmalı'}</FormValidationMessage>

        <FormLabel>Telefon 2</FormLabel>
        <FormInput
          value = {this.state.tel2}
          onChangeText={(tel2) => this.setState({tel2})}/>

        <FormLabel>Doğum Tarihi</FormLabel>
        <FormInput
          value = {this.state.birthDay}
          onChangeText={(birthDay) => this.setState({birthDay})}/>

          <DatePicker
              style={{width: 300}}
              date={this.state.date}
              locale={'tr'}
              mode="date"
              placeholder="Tarih Seç"
              format="DD-MM-YYYY"
              confirmBtnText="Onayla"
              cancelBtnText="İptal"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 30
                }
                // ... You can check the source to find the other keys.
              }}
              onDateChange={(birthDay) => {this.setState({birthDay: birthDay})}}
      />

          <TouchableOpacity style ={ styles.btn}
                            onPress = {this.saveUser}>
                            <Text style ={styles.header}>Kaydet</Text>
                            </TouchableOpacity>


        </View></ScrollView>
      </KeyboardAvoidingView>

    )
  }

}
const styles = StyleSheet.create({
  wrapper: {
    flex:1,
    justifyContent: 'space-between'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F6F8E2',
    paddingLeft: 40,
    paddingRight: 40,
  },

  btn: {
    alignSelf: 'stretch',
    alignItems:'center',
    padding: 15,
    backgroundColor :'#F6F8E2',

  },
  header :{
    fontSize: 20,
    marginBottom: 30,
    color: '#333333',
    fontWeight: 'bold'
  }

});
