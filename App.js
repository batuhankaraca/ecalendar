/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  Text,
  View,SearchBar, Icon,Header, Item, Input,Button
} from 'native-base';
import {StyleSheet} from 'react-native';

import { StackNavigator, createBottomTabNavigator, createStackNavigator} from 'react-navigation';
import Login from './app/components/Login';
import Home from './app/components/Home';
import Dukkan from './app/components/Dukkan';
import Takvim from './app/components/Takvim';
import Setting from './app/components/Setting';
// import Listeleme from './app/components/Listeleme';



let _HomeStack = createStackNavigator  ({firstScreen : Home, Dukkan: Dukkan, Takvim:Takvim, Setting:Setting});

let _Login  = createStackNavigator ({screen : Login});
let _tabCofig     =
  {
    navigationOptions: ({ navigation }) => ({
      swipeEnabled: true,
      animationEnabled : true,
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName = '';
        switch (routeName) {
          case 'Home':
            iconName = 'ios-home';
            break;
          // case 'Login':
          //   iconName = 'ios-person';
          //   break;
        }
        return <Icon  name={iconName} size={20} color={tintColor} />;

      },
    }),
    tabBarOptions: {

      inactiveBackgroundColor: '#F6F8E2',
      activeBackgroundColor: '#F6F8E2',
      alignItems:'Right',
      showLabel: false,
      swipeEnabled:true
    },
  };
let AppStack      = createStackNavigator ({ Home: _HomeStack, /*Login: _Login*/ });




export default class App extends React.Component {

  render(){

    return(

     <_HomeStack/>
    )
  }
}
const styles = StyleSheet.create({
  view: {

    backgroundColor: '#95a5a6',
  }
})
